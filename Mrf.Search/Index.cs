﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Tokenattributes;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Function;
using Lucene.Net.Store;
using Lucene.Net.Util;
using MathNet.Numerics;
using sharonjl.utils;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Device.Location;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace BuiltByFoo.Common.Search
{
	public class Index
	{
		private Directory _directory;
		private IndexWriter _writer;
		public const string ID_FIELD_NAME = "__id";
		public const string LOCATION_HASH_FIELD_NAME = "Location.Hash";
		public const string LOCATION_FIELD_NAME = "Location";
		private Lucene.Net.Analysis.Analyzer _defaultAnalyzer;
		private Lucene.Net.QueryParsers.QueryParser _parser;
		private IEnumerable<Field> _fields;
		private Analyzer _analyzer;
		private int _maxScanDocs = 500;

		public Index(Directory directory, IEnumerable<Field> fieldList)
		{
			_fields = fieldList;

			var boosts = new Dictionary<string, float>();
			foreach (var field in fieldList)
			{
				boosts.Add(field.Name, field.Boost);
			}
			_parser = new Lucene.Net.QueryParsers.MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_30, fieldList.Select(field => field.Name).ToArray(), this.GetAnalyzer(), boosts);
			_defaultAnalyzer = new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
			_directory = directory;
			_analyzer = this.GetAnalyzer();

			if (!Lucene.Net.Index.IndexReader.IndexExists(_directory))
			{
				_writer = new Lucene.Net.Index.IndexWriter(_directory, _analyzer, true, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);
			}
			else
			{
				try
				{
					_writer = new Lucene.Net.Index.IndexWriter(_directory, _analyzer, false, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);
				}
				catch (Lucene.Net.Store.LockObtainFailedException ex)
				{
					_directory.DeleteFile("write.lock");
					_writer = new Lucene.Net.Index.IndexWriter(_directory, _analyzer, false, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);
				}
			}
			_writer.UseCompoundFile = false;
		}

        public Lucene.Net.Analysis.Analyzer GetAnalyzerByTypeName(DataType type)
        {
            if (type == DataType.TextEnglish)
            {
                return new Lucene.Net.Analysis.Snowball.SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English");
            }
            else if (type == DataType.SingleValue)
            {
                return new Lucene.Net.Analysis.KeywordAnalyzer();
            }
            else if (type ==  DataType.SimpleText)
            {
                return new Lucene.Net.Analysis.SimpleAnalyzer();
            }
            else if (type == DataType.StandardText)
            {
                return new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            }
            else if (type == DataType.TextWithPunctuation)
            {
                return new Lucene.Net.Analysis.WhitespaceAnalyzer();
            }
            else if (type == DataType.MultiValue)
            {
                return new NewLineAnalyzer();
            }
            else
            {
                //return new Lucene.Net.Analysis.WhitespaceAnalyzer();
                return new Lucene.Net.Analysis.Snowball.SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English");
            }
        }

        public Lucene.Net.Analysis.Analyzer GetAnalyzer()
        {
            Lucene.Net.Analysis.PerFieldAnalyzerWrapper analyzer = new Lucene.Net.Analysis.PerFieldAnalyzerWrapper(_defaultAnalyzer);
            foreach (var fieldProperty in _fields)
            {
                analyzer.AddAnalyzer(fieldProperty.Name, GetAnalyzerByTypeName(fieldProperty.DataType));
            }
            return analyzer;
        }

        public void AddDocument(Document document)
        {
            var indexedDocument = new Lucene.Net.Documents.Document();

            indexedDocument.Add(new Lucene.Net.Documents.Field(ID_FIELD_NAME, document.Id, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.NO));
			indexedDocument.Add(new Lucene.Net.Documents.Field("Title", document.Title, Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.NO));
			indexedDocument.Add(new Lucene.Net.Documents.Field("Description", document.Description, Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.NO));

			if (document.Latitude.HasValue && document.Longitude.HasValue)
			{
				// calculate and store hashes based on different levels of precision:
				// 3 - region
				// 4 - county
				// 5 - city
				// 6 - ~ 2.59 square km
				// 7 - ~ 3 blocks
				var geohashes = new List<string>();
				for (var i = 1; i < 8; i++)
				{
					geohashes.Add(Geohash.Encode(document.Latitude.Value, document.Longitude.Value, i));
				}
				var geohashTokenStream = new PropertySetTokenStream(geohashes.ToArray());
				indexedDocument.Add(new Lucene.Net.Documents.Field(LOCATION_HASH_FIELD_NAME, geohashTokenStream));
				indexedDocument.Add(new Lucene.Net.Documents.Field(LOCATION_FIELD_NAME, document.Latitude.Value.ToString("N4") + ":" + document.Longitude.Value.ToString("N4"), Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NO));
			}



            foreach (var kv in document.Values())
            {
                if (kv.Value.GetType() == typeof(DateTime))
                {
                    indexedDocument.Add(new Lucene.Net.Documents.NumericField(kv.Key).SetIntValue((int)document.ReadDateTime(kv.Key).ToOADate()));
                } 
                else if (kv.Value.GetType() == typeof(int))
                {
                    indexedDocument.Add(new Lucene.Net.Documents.NumericField(kv.Key).SetIntValue(document.ReadInt(kv.Key)));
                } 
                else if (kv.Value.GetType() == typeof(string[]))
                {
                    var values = string.Join("\n", document.ReadValues(kv.Key));
                    var buffer = new System.IO.StringReader(string.Join("\n", document.ReadValues(kv.Key)));
                    var tokenizer = new NewLineTokenizer(buffer);
                    //indexedDocument.Add(new Lucene.Net.Documents.Field(kv.Key, string.Join("\n", document.ReadValues(kv.Key)), Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.YES));
                    var mvf = new Lucene.Net.Documents.Field(kv.Key, values, Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
                    mvf.SetTokenStream(tokenizer);
                    indexedDocument.Add(mvf);
                }
                else
                {
                    indexedDocument.Add(new Lucene.Net.Documents.Field(kv.Key, document.ReadString(kv.Key), Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES));
                }
            }

            this._writer.UpdateDocument(new Lucene.Net.Index.Term(ID_FIELD_NAME, document.Id), indexedDocument);
        }

        public void DeleteDocument(string id)
        {
            this._writer.DeleteDocuments(new Lucene.Net.Index.Term(ID_FIELD_NAME, id));
        }

        public Lucene.Net.Search.Query PrepareKeywordQuery(string searchText)
        {
            lock (_parser)
            {
				if (string.IsNullOrEmpty(searchText))
				{
					return null;
				}
				else
				{
					return _parser.Parse(searchText);
				}
            }
        }

        internal class DocumentScanResults
        {
            internal HashSet<int> InternalIds { get; set; }
			internal List<string> ExternalIds { get; set; }
			internal List<float> Scores { get; set; }
            internal List<Facet> Facets { get; set; }
			internal List<float> Distances { get; set; }
		}

        private DocumentScanResults scanDocuments(IndexSearcher searcher, Query query, double? latitude, double? longitude, int start, int topN, IEnumerable<string> fieldsToLoad, IEnumerable<Filter> filters)
        {
            var ignoreFilters = new HashSet<string>();
            foreach (var filter in filters)
            {
                ignoreFilters.Add(filter.FieldName + "." + filter.Label);
            }

			var collector = Lucene.Net.Search.TopFieldCollector.Create(new Lucene.Net.Search.Sort(new Lucene.Net.Search.SortField(null, Lucene.Net.Search.SortField.SCORE)), _maxScanDocs, false, true, true, true);

			searcher.Search(query, collector);
            
            var facetMapper = new FacetMapper(fieldsToLoad, ignoreFilters);

            var topDocs = collector.TopDocs();
            var internalIds = new HashSet<int>();
            var externalIds = new List<string>();
			var scores = new List<float>();
			var distances = new List<float>();

			GeoCoordinate queryCoords = null;
			
			if (latitude.HasValue && longitude.HasValue)
			{
				queryCoords = new GeoCoordinate(latitude.Value, longitude.Value);
			}
				
            for (var i = 0; i < collector.TotalHits && i < _maxScanDocs; i++)
            {
                var docId = topDocs.ScoreDocs[i].Doc;
                internalIds.Add(docId);
                searcher.IndexReader.GetTermFreqVector(docId, facetMapper);
                if (i >= start && i < start + topN && i < collector.TotalHits)
                {
                    var indexedDocument = searcher.Doc(docId);
					var id = indexedDocument.Get(ID_FIELD_NAME);
					
					if (queryCoords != null)
					{
						var latLng = indexedDocument.Get(LOCATION_FIELD_NAME);
						if (string.IsNullOrEmpty(latLng) == false)
						{
							var ll = latLng.Split(':');
							var storedCoords = new GeoCoordinate(double.Parse(ll[0]), double.Parse(ll[1]));
							var distanceInKm = (float)queryCoords.GetDistanceTo(storedCoords) / 1000;
							distances.Add(distanceInKm);
						}
					}

					externalIds.Add(id);
					scores.Add(topDocs.ScoreDocs[i].Score);
				}
            }

            return new DocumentScanResults()
            {
                InternalIds = internalIds,
                Facets = facetMapper.GetFacets().ToList(),
				ExternalIds = externalIds,
				Scores = scores,
				Distances = distances
            };
        }

        private void getRangeFacets(DocumentScanResults results, Query query, IndexSearcher searcher, IEnumerable<Filter> filters)
        {
            var facets = results.Facets;
            var docs = results.InternalIds;

            var filter = new Lucene.Net.Search.QueryWrapperFilter(query);
            var ranges = _fields.Where(field => field.IsBrowsable && field.Ranges != null && field.Ranges.Count() > 0).ToList();
            foreach (var field in ranges)
            {
                var facet = new Facet() { Name = field.Name };
                var values = new List<FacetValue>();
                if (field.DataType == DataType.Numeric)
                {
                    foreach (var range in field.Ranges)
                    {
						var label = range.Lower + " to " + range.Upper;
						if (filters.Any(F => F.FieldName == field.Name && F.Label == label))
						{
							continue;
						}
						int? lower = null;
						int? upper = null;
						if (range.Lower > 0)
						{
							lower = range.Lower;
						}
						if (range.Upper > 0)
						{
							upper = range.Upper;
						}
                        var nq = Lucene.Net.Search.NumericRangeQuery.NewIntRange(field.Name, lower, upper, true, true);
                        //var bq = new Lucene.Net.Search.BooleanQuery();
                        //bq.Add(filter, Lucene.Net.Search.Occur.MUST);
                        //bq.Add(nq, Lucene.Net.Search.Occur.MUST);
                        var hitCount = 0;
						var r = searcher.Search(nq, filter, _maxScanDocs);
						hitCount = r.TotalHits;
						//foreach (var hit in .ScoreDocs)
						//{
						//	if (docs.Contains(hit.Doc))
						//	{
						//		hitCount++;
						//	}
						//}
                        if (hitCount > 0)
                        {
							if (hitCount > _maxScanDocs)
							{
								hitCount = _maxScanDocs;
							}
                            values.Add(new FacetValue() { Count = hitCount, Value = range.Lower + " to " + range.Upper, Lower = range.Lower, Upper = range.Upper });
                        }
                        facet.IsRange = true;
                    }
                }
                facet.Values = values.ToArray();
                if (values.Count > 0)
                {
                    facets.Add(facet);
                }
            }
            facets.RemoveAll(F => F.Values.Length == 0);
        }

		public SearchResult Search(Lucene.Net.Search.Query query, int start, int topN, IEnumerable<string> fieldsToLoad, IEnumerable<Filter> filters, IEnumerable<GuassianValue> gaussianFields, double? latitude = null, double? longitude = null)
        {
            using (var searcher = new IndexSearcher(_directory, true))
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

				var gaussianScorer = new GuassianFieldScorer(query, gaussianFields.Select(G => new FieldScoreQuery(G.FieldName, FieldScoreQuery.Type.INT)).ToArray(), gaussianFields.Select(G => G.Value).ToArray(), gaussianFields.Select(G => G.Weight).ToArray());

                var results = this.scanDocuments(searcher, gaussianScorer, latitude, longitude, start, topN, fieldsToLoad, filters);
                
				this.getRangeFacets(results, query, searcher, filters);

                stopwatch.Stop();
                return new SearchResult()
                {
                    Milliseconds = stopwatch.ElapsedMilliseconds,
                    Facets = results.Facets.ToArray(),
                    Start = start,
                    Stop = Math.Min(start + topN, results.InternalIds.Count),
                    Total = results.InternalIds.Count,
                    DocumentIds = results.ExternalIds.ToArray(),
					Scores = results.Scores.ToArray(),
					Distances = results.Distances.ToArray()
                };
            }
        }

        public void Commit()
        {
			this._writer.Commit();
        }

        public void Optimize()
        {
            this._writer.Optimize();
        }

        public void Dispose()
        {
			this._writer.Dispose();
			this._directory.Dispose();
		}

        public class ParseQueryStringResults
        {
            public List<Filter> Filters { get; set; }
            public List<string> FieldsToLoad { get; set; }
            public string QueryString { get; set; }
            public Query Query { get; set; }
			public double? Latitude { get; set; }
			public double? Longitude { get; set; }
			public List<GuassianValue> GuassianValues { get; set; }
        }

		public Query PrepareLocationQuery(double latitude, double longitude, int precision)
		{
			var hash = Geohash.Encode(latitude, longitude, precision);

			// calculate bounding box
			var left = Geohash.CalculateAdjacent(hash, Geohash.Direction.Left);
			var top = Geohash.CalculateAdjacent(hash, Geohash.Direction.Top);
			var bottom = Geohash.CalculateAdjacent(hash, Geohash.Direction.Bottom);
			var right = Geohash.CalculateAdjacent(hash, Geohash.Direction.Right);
			var upperLeft = Geohash.CalculateAdjacent(left, Geohash.Direction.Top);
			var bottomLeft = Geohash.CalculateAdjacent(left, Geohash.Direction.Bottom);
			var upperRight = Geohash.CalculateAdjacent(right, Geohash.Direction.Top);
			var bottomRight = Geohash.CalculateAdjacent(right, Geohash.Direction.Bottom);

			var boundingBox = new Term[] { 
					new Term(LOCATION_HASH_FIELD_NAME, left), 
					new Term(LOCATION_HASH_FIELD_NAME, top),
					new Term(LOCATION_HASH_FIELD_NAME, right), 
					new Term(LOCATION_HASH_FIELD_NAME, bottom), 
					new Term(LOCATION_HASH_FIELD_NAME, upperLeft), 
					new Term(LOCATION_HASH_FIELD_NAME, upperRight), 
					new Term(LOCATION_HASH_FIELD_NAME, bottomLeft),
					new Term(LOCATION_HASH_FIELD_NAME, bottomRight)
				};

			var bq = new BooleanQuery();
			foreach (var term in boundingBox)
			{
				bq.Add(new TermQuery(term), Occur.SHOULD);
			}

			return bq;
		}

		public Query PrepareLocationQuery(NameValueCollection queryString, out double? latitude, out double? longitude)
		{
			var locationQS = queryString["_l"];
			
			latitude = null;
			longitude = null;

			if (string.IsNullOrEmpty(locationQS) == false)
			{
				var location = locationQS.Split(':');
				latitude = double.Parse(location[0]);
				longitude = double.Parse(location[1]);
				var locationPrecision = 5;
				var locationPrecisionQS = queryString["_lp"];
				if (string.IsNullOrEmpty(locationPrecisionQS) == false)
				{
					locationPrecision = int.Parse(locationPrecisionQS);
				}

				return PrepareLocationQuery(latitude.Value, longitude.Value, locationPrecision);
			}

			return null;
		}

        public ParseQueryStringResults ParseQueryString(string queryString)
        {
            var oldQs = HttpUtility.ParseQueryString(queryString);
            var queryText = oldQs["q"];
            var keywordQuery = this.PrepareKeywordQuery(queryText);
            var query = new BooleanQuery();
			query.Add(new BooleanClause(keywordQuery, Occur.MUST));

			double? latitude = null;
			double? longitude = null;
			
			var locationQuery = this.PrepareLocationQuery(oldQs, out latitude, out longitude);
			if (locationQuery != null)
			{
				query.Add(locationQuery, Occur.MUST);
			}

            var filters = new List<Filter>();
            var fieldsToLoad = new List<string>();
			var gaussianFields = new List<GuassianValue>();

            foreach (string name in oldQs.Keys)
            {
                if (string.IsNullOrEmpty(name))
                {
                    continue;
                }
                if (name != "c" && name != "q" && name.StartsWith("_") == false)
                {
                    var facetQuery = new BooleanQuery();
                    foreach (var value in oldQs.GetValues(name))
                    {
                        var queryParam = "";
						if (value.Contains(":"))
						{
							var guassian = value.Split(':');
							float guassianValue = 0.0f;
							int guassianWeight = 2;
							if (float.TryParse(guassian[0], out guassianValue) && int.TryParse(guassian[1], out guassianWeight))
							{
								gaussianFields.Add(new GuassianValue(name, guassianValue, guassianWeight));
							}
							queryParam = HttpUtility.UrlEncode(name) + "=" + guassian[0] + ":" + guassian[1];
						}
                        else if (_fields.FirstOrDefault(F => F.Name == name && F.Ranges != null && F.Ranges.Count() > 0) != null && value.Contains("-"))
                        {
                            var range = value.Split('-');
                            var nq = Lucene.Net.Search.NumericRangeQuery.NewIntRange(name, int.Parse(range[0]), int.Parse(range[1]), true, true);
                            facetQuery.Add(new BooleanClause(nq, Occur.MUST));
                            queryParam = HttpUtility.UrlEncode(name) + "=" + range[0] + "-" + range[1];
							query.Add(new BooleanClause(facetQuery, Occur.MUST));
                        }
                        else
                        {
                            facetQuery.Add(new BooleanClause(new TermQuery(new Lucene.Net.Index.Term(name, value)), Occur.MUST));
                            queryParam = HttpUtility.UrlEncode(name) + "=" + HttpUtility.UrlEncode(value);
							query.Add(new BooleanClause(facetQuery, Occur.MUST));
                        }
						filters.Add(new Filter() { FieldName = name, Label = value, Url = queryParam });
                    }
                } 
                else if (name == "_f[]")
                {
                    foreach (var value in oldQs.GetValues(name))
                    {
                        fieldsToLoad.Add(value);
                    }
                }
            }

            var allQs = new List<string>();
            var filterQs = new List<string>();
            // create query strings for each filter
            for (var x = 0; x < filters.Count; x++)
            {
                var f = new List<string>();
                for (var y = 0; y < filters.Count; y++)
                {
                    // generate qs that includes other filters but excludes this one
                    if (x != y)
                    {
                        f.Add(filters[y].Url);
                    }
                }

                filterQs.Add(string.Join("&", f));
                allQs.Add(filters[x].Url);
            }

            for (var x = 0; x < filters.Count; x++)
            {
                filters[x].Url = "&q=" + HttpUtility.UrlEncode(queryText) +
                    "&c=" + oldQs["c"] +
                    "&" + string.Join("&", filterQs[x]);
            }
            allQs.Add("q=" + HttpUtility.UrlEncode(queryText));
            allQs.Add("c=" + oldQs["c"]);

			filters.RemoveAll(F => gaussianFields.Any(G => G.FieldName == F.FieldName));

            return new ParseQueryStringResults()
            {
                Filters = filters,
                QueryString = string.Join("&", allQs.Select(f => f)),
                Query = query,
                FieldsToLoad = fieldsToLoad.Distinct().ToList(),
				Latitude = latitude,
				Longitude = longitude,
				GuassianValues = gaussianFields
            };
        }
        
        public IEnumerable<string> Facets()
        {
			using (var reader = this._writer.GetReader())
			{
				return reader
					.GetFieldNames(Lucene.Net.Index.IndexReader.FieldOption.INDEXED_WITH_TERMVECTOR)
					.Where(N => N != "Content")
					.ToArray();
			}
        }
	}

	public enum DataType
	{
		SingleValue,
		MultiValue,
		TextEnglish,
		SimpleText,
		StandardText,
		Numeric,
		DateTime,
		TextWithPunctuation
	}

	public class Range
	{
		public int Lower { get; set; }
		public int Upper { get; set; }
	}

	public class GuassianValue
	{
		public string FieldName { get; private set; }
		public float Value { get; private set; }
		public int Weight { get; private set; }

		public GuassianValue(string fieldName, float value, int weight)
		{
			this.FieldName = fieldName;
			this.Value = value;
			this.Weight = weight;
		}
	}

	public class Field
	{
		public string Name { get; private set; }
		public DataType DataType { get; private set; }
		public float Boost { get; private set; }
		public int Order { get; private set; }
		public bool IsBrowsable { get; private set; }
		public IEnumerable<Range> Ranges { get; private set; }

		public Field(string name, DataType dataType, float boost, int order, bool isBrowsable)
		{
			this.Name = name;
			this.DataType = dataType;
			this.Boost = boost;
			this.Order = order;
			this.IsBrowsable = isBrowsable;
		}

		public Field(string name, DataType dataType, float boost, int order, bool isBrowsable, IEnumerable<Range> ranges)
			: this(name, dataType, boost, order, isBrowsable)
		{
			this.Ranges = ranges;
		}
	}

	public class Document
	{
		public string Id { get; set; }
		public double? Latitude { get; set; }
		public double? Longitude { get; set; }

		private Dictionary<string, object> values = new Dictionary<string, object>();

		public void AddField(string name, string value)
		{
			this.values.Add(name, value);
		}

		public void AddField(string name, string[] values)
		{
			this.values.Add(name, values);
		}

		public void AddField(string name, int value)
		{
			this.values.Add(name, value);
		}

		public void AddField(string name, DateTime value)
		{
			this.values.Add(name, value);
		}

		public string ReadString(string fieldName)
		{
			if (this.values.ContainsKey(fieldName))
			{
				return this.values[fieldName] as string;
			}
			else
			{
				return string.Empty;
			}
		}

		public string[] ReadValues(string fieldName)
		{
			if (this.values.ContainsKey(fieldName))
			{
				return this.values[fieldName] as string[];
			}
			else
			{
				return new string[0];
			}
		}

		public int ReadInt(string fieldName)
		{
			if (this.values.ContainsKey(fieldName))
			{
				return Convert.ToInt32(this.values[fieldName]);
			}
			else
			{
				return 0;
			}
		}

		public DateTime ReadDateTime(string fieldName)
		{
			if (this.values.ContainsKey(fieldName))
			{
				return Convert.ToDateTime(this.values[fieldName]);
			}
			else
			{
				return DateTime.MinValue;
			}
		}

		public IEnumerable<KeyValuePair<string, object>> Values()
		{
			return this.values.ToArray();
		}



		public string Title { get; set; }

		public string Description { get; set; }
	}

	public class NewLineAnalyzer : Analyzer
	{
		public override TokenStream TokenStream(string fieldName, System.IO.TextReader reader)
		{
			return new NewLineTokenizer(reader);
		}
	}

	public class NewLineTokenizer : CharTokenizer
	{
		public NewLineTokenizer(System.IO.TextReader @in)
			: base(@in)
		{
		}

		public NewLineTokenizer(AttributeSource source, System.IO.TextReader @in)
			: base(source, @in)
		{
		}

		public NewLineTokenizer(AttributeFactory factory, System.IO.TextReader @in)
			: base(factory, @in)
		{
		}

		protected override bool IsTokenChar(char c)
		{
			return c != '\n';
		}
	}

	public class FacetMapper : Lucene.Net.Index.TermVectorMapper
	{
		private Dictionary<string, Dictionary<string, int>> facets = new Dictionary<string, Dictionary<string, int>>();
		private Dictionary<string, int> currentField;
		private int docCount = 0;
		private HashSet<string> ignoreFilters;
		private string currentFieldName;

		public FacetMapper(IEnumerable<string> fields, HashSet<string> ignoreFilters)
		{
			foreach (var field in fields)
			{
				this.facets.Add(field, new Dictionary<string, int>());
			}
			this.ignoreFilters = ignoreFilters;
		}

		public override void SetExpectations(string field, int numTerms, bool storeOffsets, bool storePositions)
		{
			if (this.facets.ContainsKey(field) == false)
			{
				this.currentField = null;
				this.currentFieldName = null;
			}
			else
			{
				this.currentField = facets[field];
				this.currentFieldName = field;
			}
		}

		public override void Map(string term, int frequency, Lucene.Net.Index.TermVectorOffsetInfo[] offsets, int[] positions)
		{
			if (this.currentField != null && string.IsNullOrEmpty(term) == false && this.ignoreFilters.Contains(this.currentFieldName + "." + term) == false)
			{
				if (this.currentField.ContainsKey(term) == false)
				{
					this.currentField.Add(term, 0);
				}
				this.currentField[term]++;
			}
		}

		public override void SetDocumentNumber(int documentNumber)
		{
			docCount++;
			base.SetDocumentNumber(documentNumber);
		}

		public IEnumerable<Facet> GetFacets()
		{
			foreach (var facet in this.facets)
			{
				yield return new Facet()
				{
					Name = facet.Key,
					Values = facet.Value.Select(value => new FacetValue() { Value = value.Key, Count = value.Value }).ToArray()
				};
			}
		}
	}

	public class Facet
	{
		public string Name { get; set; }
		public FacetValue[] Values { get; set; }
		public bool IsRange { get; set; }
	}

	public class FacetValue
	{
		public string Value { get; set; }
		public int Count { get; set; }
		public int Lower { get; set; }
		public int Upper { get; set; }
	}

	public class Filter
	{
		public string FieldName { get; set; }
		public string Label { get; set; }
		public string Url { get; set; }
	}

	public class SearchResult
	{
		public long Milliseconds { get; set; }
		public Facet[] Facets { get; set; }
		public int Start { get; set; }
		public int Stop { get; set; }
		public string[] DocumentIds { get; set; }
		public float[] Scores { get; set; }
		public int Total { get; set; }
		public float[] Distances { get; set; }
	}

	public class PropertySetTokenStream : TokenStream
	{
		private string[] _tokens;
		private ITermAttribute _termAttr;
		private int _currentPosition = -1;

		public PropertySetTokenStream(string[] tokens)
		{
			_tokens = tokens;
			_termAttr = AddAttribute<ITermAttribute>();
		}

		protected override void Dispose(bool disposing)
		{

		}

		public override bool IncrementToken()
		{
			ClearAttributes();
			_currentPosition++;

			if (_tokens.Length == 0 || _currentPosition == _tokens.Length)
			{
				return false;
			}

			_termAttr.SetTermBuffer(_tokens[_currentPosition]);
			_termAttr.SetTermLength(_tokens[_currentPosition].Length);

			return _currentPosition != _tokens.Length;
		}

		public override void Reset()
		{
			base.Reset();
			_currentPosition = -1;
		}
	}

	public class GuassianFieldScorer : CustomScoreQuery
	{
		private float[] _targetValues;
		private int[] _weights;
		private double[] _target;

		public GuassianFieldScorer(Query subquery, ValueSourceQuery[] queries, float[] targetValues, int[] weights) : base(subquery, queries)
		{
			this.SetStrict(true);
			this._targetValues = targetValues;
			this._weights = weights;
			this._target = new double[weights.Length];
			for (var i = 0; i < this._target.Length; i++)
			{
				this._target[i] = 1.0f;
			}
		}

		public override float CustomScore(int doc, float subQueryScore, float[] valSrcScores)
		{
			var document = new double[valSrcScores.Length];
			
			for (var i = 0; i < valSrcScores.Length; i++)
			{
				var distance = Math.Min(_targetValues[i], valSrcScores[i]) / Math.Max(_targetValues[i], valSrcScores[i]);
				document[i] = Math.Pow(distance, _weights[i]);
			}

			if (document.Length == 0)
			{
				return subQueryScore;
			}
			else
			{
				//return (float)(subQueryScore * document.Average());
				return (float)(Math.Pow(document.Average(), 2));
				//return (float)document.Average();
			}
		}
	}
}


/**
 *  Copyright (C) 2011 by Sharon Lourduraj
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  ------------------------------------------------------------------------------
 *  
 *  This code is a direct derivation from:
 *      GeoHash Routines for Javascript 2008 (c) David Troy. 
 *  The source of which can be found at: 
 *      https://github.com/davetroy/geohash-js
 */

namespace sharonjl.utils
{
	// HACK: ugly bit of code to simplify the syntax for the neighbor/boundary sets
	public class NestedHashset
	{
		private Dictionary<string, NestedHashset> _values = new Dictionary<string, NestedHashset>();

		public NestedHashset()
		{

		}

		public NestedHashset this[string key]
		{
			get
			{
				if (_values.ContainsKey(key) == false)
				{
					_values.Add(key, new NestedHashset());
				}
				return _values[key];
			}
			set
			{
				_values[key] = value;
			}
		}

		public string Value { get; set; }

		public static implicit operator NestedHashset(string value)
		{
			var n = new NestedHashset();
			n.Value = value;
			return n;
		}
	}

	public static class Geohash
	{
		public enum Direction
		{
			Left,
			Right,
			Top,
			Bottom
		}

		private static int[] BITS = new int[] { 16, 8, 4, 2, 1 };
		private static string BASE32 = "0123456789bcdefghjkmnpqrstuvwxyz";
		private static NestedHashset NEIGHBORS = new NestedHashset();
		private static NestedHashset BORDERS = new NestedHashset();
		
		static Geohash()
		{
			NEIGHBORS["right"]["even"] = "bc01fg45238967deuvhjyznpkmstqrwx";
			NEIGHBORS["left"]["even"] = "238967debc01fg45kmstqrwxuvhjyznp";
			NEIGHBORS["top"]["even"] = "p0r21436x8zb9dcf5h7kjnmqesgutwvy";
			NEIGHBORS["bottom"]["even"] = "14365h7k9dcfesgujnmqp0r2twvyx8zb";

			BORDERS["right"]["even"] = "bcfguvyz";
			BORDERS["left"]["even"] = "0145hjnp";
			BORDERS["top"]["even"] = "prxz";
			BORDERS["bottom"]["even"] = "028b";

			NEIGHBORS["bottom"]["odd"] = NEIGHBORS["left"]["even"];
			NEIGHBORS["top"]["odd"] = NEIGHBORS["right"]["even"];
			NEIGHBORS["left"]["odd"] = NEIGHBORS["bottom"]["even"];
			NEIGHBORS["right"]["odd"] = NEIGHBORS["top"]["even"];

			BORDERS["bottom"]["odd"] = BORDERS["left"]["even"];
			BORDERS["top"]["odd"] = BORDERS["right"]["even"];
			BORDERS["left"]["odd"] = BORDERS["bottom"]["even"];
			BORDERS["right"]["odd"] = BORDERS["top"]["even"];
		}

		private static void refine_interval(ref double[] interval, int cd, int mask)
		{
			if ((cd & mask) != 0)
				interval[0] = (interval[0] + interval[1]) / 2;
			else
				interval[1] = (interval[0] + interval[1]) / 2;
		}

		public static double[] Decode(string geohash)
		{
			var is_even = true;
			var lat = new double[3];
			var lon = new double[3];
			var mask = 0;

			lat[0] = -90.0; lat[1] = 90.0;
			lon[0] = -180.0; lon[1] = 180.0;
			var lat_err = 90.0; var lon_err = 180.0;

			for (var i = 0; i < geohash.Length; i++)
			{
				var c = geohash[i];
				var cd = BASE32.IndexOf(c);
				for (var j = 0; j < 5; j++)
				{
					mask = BITS[j];
					if (is_even)
					{
						lon_err /= 2;
						refine_interval(ref lon, cd, mask);
					}
					else
					{
						lat_err /= 2;
						refine_interval(ref lat, cd, mask);
					}
					is_even = !is_even;
				}
			}
			lat[2] = (lat[0] + lat[1]) / 2;
			lon[2] = (lon[0] + lon[1]) / 2;

			return new double[] { lat[2], lon[2] };		
		}

		public static string Encode(double latitude, double longitude, int precision = 12)
		{
			var is_even = true;
			var lat = new double[2];
			var lon = new double[2];
			var bit = 0;
			var ch = 0;
			var geohash = "";
			var mid = 0.0;

			lat[0] = -90.0; lat[1] = 90.0;
			lon[0] = -180.0; lon[1] = 180.0;

			while (geohash.Length < precision)
			{
				if (is_even)
				{
					mid = (lon[0] + lon[1]) / 2;
					if (longitude > mid)
					{
						ch |= BITS[bit];
						lon[0] = mid;
					}
					else
						lon[1] = mid;
				}
				else
				{
					mid = (lat[0] + lat[1]) / 2;
					if (latitude > mid)
					{
						ch |= BITS[bit];
						lat[0] = mid;
					}
					else
						lat[1] = mid;
				}

				is_even = !is_even;
				if (bit < 4)
					bit++;
				else
				{
					geohash += BASE32[ch];
					bit = 0;
					ch = 0;
				}
			}
			return geohash;
		}

		public static string CalculateAdjacent(string hash, Direction direction)
		{
			var dir = direction.ToString().ToLower();
			var srcHash = hash.ToLower();
			var lastChr = srcHash[srcHash.Length - 1];
			var type = srcHash.Length % 2 == 0 ? "odd" : "even";
			var @base = srcHash.Substring(0, srcHash.Length - 1);
			if (BORDERS[dir][type].Value.IndexOf(lastChr) != -1)
				@base = CalculateAdjacent(@base, direction);
			return @base + BASE32[NEIGHBORS[dir][type].Value.IndexOf(lastChr)];
		}
	}
}