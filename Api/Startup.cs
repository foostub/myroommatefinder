﻿using System;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.Owin;
//using Owin;
using Castle.Windsor;
using Api.Windsor;
using Owin;
using System.Configuration;
using Lucene.Net.Store;
using Castle.MicroKernel.Registration;
using Api.Services.Search;
using Api.Services.Storage;
using System.Web.Http.Controllers;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;
using Microsoft.Owin.Security.OAuth;
using Api.Services.Authentication;
using Microsoft.Owin.Security.Facebook;
using System.Threading.Tasks;
using System.Security.Claims;
using BuiltByFoo.Common.Search;

[assembly: OwinStartup(typeof(Api.Startup))]
namespace Api
{
	public partial class Startup
	{

		public void Configuration(IAppBuilder app)
		{
			var config = new HttpConfiguration();

			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "v1/api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

			var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
			jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

			var container = new WindsorContainer();

			var searchPath = System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/similarity");//ConfigurationManager.AppSettings["Search.Path"];
			var directory = FSDirectory.Open(searchPath);
			var index = new Index(directory, new Field[] 
			{
				new Field("Title", DataType.TextEnglish, 1.0f, 1, false),
				new Field("Description", DataType.TextEnglish, 1.0f, 2, false),
				new Field("Gender", DataType.SingleValue, 1.0f, 3, true),
				new Field("Orientation", DataType.SingleValue, 1.0f, 4, true),
				new Field("Birthday", DataType.DateTime, 1.0f, 5, false),
				new Field("MoveInBy", DataType.DateTime, 1.0f, 6, false),
				new Field("MaxRent", DataType.Numeric, 1.0f, 7, true, new Range[] 
				{
					new Range() { Lower = 200, Upper = 300 },
					new Range() { Lower = 301, Upper = 400 },
					new Range() { Lower = 401, Upper = 600 },
					new Range() { Lower = 601, Upper = 750 },
					new Range() { Lower = 751, Upper = 1000 },
					new Range() { Lower = 1001, Upper = 2000 },
				}),
				new Field("MaxLease", DataType.Numeric, 1.0f, 8, true, new Range[] 
				{
					new Range() { Lower = 1, Upper = 3 },
					new Range() { Lower = 4, Upper = 6 },
					new Range() { Lower = 6, Upper = 12 }
				}),
				new Field("MaxUtilities", DataType.Numeric, 1.0f, 9, true, new Range[] 
				{
					new Range() { Lower = 25, Upper = 50 },
					new Range() { Lower = 51, Upper = 75 },
					new Range() { Lower = 75, Upper = 125 },
					new Range() { Lower = 125, Upper = 200 },
					new Range() { Lower = 201, Upper = 300 },
				}),
				new Field("MaxDeposit", DataType.Numeric, 1.0f, 10, true, new Range[] 
				{
					new Range() { Lower = 200, Upper = 300 },
					new Range() { Lower = 301, Upper = 400 },
					new Range() { Lower = 401, Upper = 600 },
					new Range() { Lower = 601, Upper = 750 },
					new Range() { Lower = 751, Upper = 1000 },
					new Range() { Lower = 1001, Upper = 2000 },
				}),
			});

			container.Register(Component.For<Index>()
				.UsingFactoryMethod(_ => index)
				.LifestyleSingleton());

			container.Register(Component.For<SearchService>()
				.ImplementedBy<SearchService>()
				.LifestyleSingleton());

			var embeddedDbPath = System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/db/profiles.db");//ConfigurationManager.AppSettings["EmbeddedDbPath"];
			container.Register(Component.For<IProfileStore>()
				.UsingFactoryMethod(_ => new EmbeddedProfileStore(embeddedDbPath))
				.LifestyleTransient());

			container.Register(Classes.FromThisAssembly()
				.BasedOn<IHttpController>()
				.LifestyleTransient());

			config.DependencyResolver = new WindsorDependencyResolver(container);

			app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
			
			var oauthServerOptions = new OAuthAuthorizationServerOptions()
			{
				AllowInsecureHttp = true,
				TokenEndpointPath = new PathString("/token"),
				AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
				Provider = new SimpleAuthorizationServerProvider()
			};

			app.UseOAuthAuthorizationServer(oauthServerOptions);

			container.Register(Component.For<OAuthAuthorizationServerOptions>()
				.UsingFactoryMethod(() => oauthServerOptions)
				.LifestyleTransient());

			var bearerOptions = new OAuthBearerAuthenticationOptions();

			container.Register(Component.For<OAuthBearerAuthenticationOptions>()
				.UsingFactoryMethod(() => bearerOptions)
				.LifestyleTransient());

			app.UseOAuthBearerAuthentication(bearerOptions);

			app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

			var facebookOAuthOptions = new FacebookAuthenticationOptions()
			{
				AppId = ConfigurationManager.AppSettings["Facebook.AppId"],
				AppSecret = ConfigurationManager.AppSettings["Facebook.AppSecret"],
				Provider = new FacebookAuthenticationProvider()
				{
					OnAuthenticated = (context) =>
					{
						context.Identity.AddClaim(new Claim("accessToken", context.AccessToken));
						
						var birthDay = context.User.Value<string>("birthday");
						var gender = context.User.Value<string>("gender");
						var firstName = context.User.Value<string>("first_name");
						var lastName = context.User.Value<string>("last_name");
						var location = context.User["location"] == null ? "" : context.User["location"].Value<string>("name");
						var provider = "Facebook";
						var id = context.Id;

						context.Identity.AddClaim(new Claim("Birthday", birthDay));
						context.Identity.AddClaim(new Claim("Gender", gender));
						context.Identity.AddClaim(new Claim("FirstName", firstName));
						context.Identity.AddClaim(new Claim("LastName", lastName));
						context.Identity.AddClaim(new Claim("Location", location));
						context.Identity.AddClaim(new Claim("Provider", provider));
						context.Identity.AddClaim(new Claim("Id", id));

						return Task.FromResult(0);
					}
				},
			};

			facebookOAuthOptions.Scope.Add("user_birthday,user_location");

			app.UseFacebookAuthentication(facebookOAuthOptions);

			app.UseWebApi(config);
		}
	}
}
