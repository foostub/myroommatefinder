﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Api.Services.Authentication
{
	public class ChallengeResult : IHttpActionResult
	{
		public string LoginProvider { get; private set; }
		public HttpRequestMessage Request { get; private set; }
		public string RedirectUrl { get; private set; }

		public ChallengeResult(string loginProvider, ApiController controller, string redirectUrl)
		{
			LoginProvider = loginProvider;
			Request = controller.Request;
			RedirectUrl = redirectUrl;
		}

		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			var properties = new AuthenticationProperties { RedirectUri = RedirectUrl };
			Request.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
			response.RequestMessage = Request;
			return Task.FromResult(response);
		}
	}
}