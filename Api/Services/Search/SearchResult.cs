﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services.Search
{
	public class SearchResult
	{
		public long Milliseconds { get; set; }
		public Facet[] Facets { get; set; }
		public int Start { get; set; }
		public int Stop { get; set; }
		public string[] DocumentIds { get; set; }
		public float[] Scores { get; set; }
		public int Total { get; set; }
		public float[] Distances { get; set; }
	}

}