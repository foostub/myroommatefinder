﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Utilities;
using Mrf.Models.Profiles;
using BuiltByFoo.Common.Search;
using Lucene.Net.Search;
using Lucene.Net.Index;

namespace Api.Services.Search
{
	public class SearchService 
	{
		private readonly Index _index;
		public static string[] FIELDS_TO_LOAD = new string[] 
		{
			"Gender", "Orientation"
		};

		public SearchService(Index index)
		{
			_index = index;
		}

		public SearchResult SearchProfiles(ProfileSearch search)
		{
			var query = new BooleanQuery();
			
			var locationQuery = _index.PrepareLocationQuery(search.Latitude, search.Longitude, 5);
			query.Add(locationQuery, Occur.MUST);

			var keywordQuery = _index.PrepareKeywordQuery(search.Keywords);
			if (keywordQuery != null)
			{
				query.Add(keywordQuery, Occur.MUST);
			}
			
			var filters = new List<BuiltByFoo.Common.Search.Filter>();
			foreach (var profileFilter in search.Filters)
			{
				filters.Add(new BuiltByFoo.Common.Search.Filter() { FieldName = profileFilter.Name, Label = profileFilter.Value });
				if ((profileFilter.Lower.HasValue && profileFilter.Lower.Value > 0) || (profileFilter.Upper.HasValue && profileFilter.Upper.Value > 0))
				{
					if (profileFilter.Lower.Value == 0)
					{
						profileFilter.Lower = null;
					}
					if (profileFilter.Upper.Value == 0)
					{
						profileFilter.Upper = null;
					}
					var rangeQuery = Lucene.Net.Search.NumericRangeQuery.NewIntRange(profileFilter.Name, profileFilter.Lower, profileFilter.Upper, true, true);
					query.Add(rangeQuery, Occur.MUST);
				}
				else
				{
					query.Add(new TermQuery(new Term(profileFilter.Name, profileFilter.Value)), Occur.MUST);
				}
			}

			if (search.MoveInBy.HasValue)
			{
				var oaDays = (int)search.MoveInBy.Value.ToOADate();
				var dateRangeQuery = Lucene.Net.Search.NumericRangeQuery.NewIntRange("MoveInBy", oaDays - 5, oaDays + 5, true, true);
				query.Add(dateRangeQuery, Occur.MUST);
			}
	
			var gaussian = new List<BuiltByFoo.Common.Search.GuassianValue>();
			if (search.Birthday.HasValue)
			{
				gaussian.Add(new GuassianValue("Birthday", (float)search.Birthday.Value.ToOADate(), 40));
			}
			if (search.SocialIndex.HasValue && search.SocialIndex.Value > 0)
			{
				gaussian.Add(new GuassianValue("SocialIndex", (float)search.SocialIndex, 4));
			}
			if (search.Smoker.HasValue && search.Smoker.Value > 0)
			{
				gaussian.Add(new GuassianValue("Smoker", (float)search.Smoker, 8));
			}
			if (search.CleanlinessIndex.HasValue && search.CleanlinessIndex.Value > 0)
			{
				gaussian.Add(new GuassianValue("Cleanliness", (float)search.CleanlinessIndex, 4));
			}
			if (search.OvernightVisitors.HasValue && search.OvernightVisitors.Value > 0)
			{
				gaussian.Add(new GuassianValue("OvernightVisitors", (float)search.OvernightVisitors, 4));
			}

			var queryResults = _index.Search(query, search.Start, 10, FIELDS_TO_LOAD, filters, gaussian, search.Latitude, search.Longitude);

			return queryResults;
		}

		public void AddProfile(Profile profile)
		{
			var document = this.convert(profile);
			_index.AddDocument(document);
			//_index.Commit();
		}

		public void RemoveProfile(Guid id)
		{
			_index.DeleteDocument(id.ToString());
			_index.Commit();
		}

		private Document convert(Profile profile)
		{
			var document = new Document();
			document.Id = profile.Id.ToString();
			document.Latitude = profile.Latitude;
			document.Longitude = profile.Longitude;
			document.Title = profile.Title;
			document.Description = profile.Description;
			document.AddField("Gender", profile.Gender.ToString());
			document.AddField("Orientation", profile.Orientation.ToString());
			document.AddField("MoveInBy", profile.MoveInBy);
			document.AddField("Birthday", profile.Birthday);
			document.AddField("MaxRent", profile.MaxRent);
			document.AddField("MaxLease", profile.MaxLease);
			document.AddField("MaxUtilities", profile.MaxUtilities);
			document.AddField("MaxDeposit", profile.MaxDeposit);
			document.AddField("SocialIndex", profile.SocialIndex);
			document.AddField("OvernightVisitors", profile.OvernightVisitors);
			document.AddField("Smoker", profile.Smoker);
			document.AddField("CarnivoreIndex", profile.CarnivoreIndex);
			document.AddField("CleanlinessIndex", profile.CleanlinessIndex);
			document.AddField("WakeUpHour", profile.WakeUpHour);
			document.AddField("ToBedHour", profile.ToBedHour);
			return document;
		}

		public void Dispose()
		{
			_index.Dispose();
		}
	}
}