﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services.Search
{
	public class Facet
	{
		public string Name { get; set; }
		public FacetValue[] Values { get; set; }
		public bool IsRange { get; set; }
	}

	public class FacetValue
	{
		public string Value { get; set; }
		public int Count { get; set; }
		public int Lower { get; set; }
		public int Upper { get; set; }
	}
}