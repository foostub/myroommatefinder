﻿using Api.Models;
using Mrf.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services.Search
{
	public class PagedDataFacets : PagedData<ScoredAccount>
	{
		public BuiltByFoo.Common.Search.Facet[] Facets { get; set; }
	}
}