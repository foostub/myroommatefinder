﻿using Mrf.Models.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services.Storage
{
	public interface IProfileStore
	{
		Profile Get(Guid id);
		IEnumerable<Profile> List(IEnumerable<Guid> ids);
		void Remove(Guid id);
		void Save(Profile profile);
		Profile Get(string provider, string id);
	}
}