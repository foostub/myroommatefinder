﻿using Mrf.Models.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services.Storage
{
	public class RAMProfileStore : IProfileStore
	{
		private List<Profile> _profiles = new List<Profile>();

		public Profile Get(Guid id)
		{
			return _profiles.FirstOrDefault(P => P.Id == id);
		}

		public IEnumerable<Profile> List(IEnumerable<Guid> ids)
		{
			return _profiles.Where(P => ids.Contains(P.Id));
		}

		public void Remove(Guid id)
		{
			_profiles.RemoveAll(P => P.Id == id);
		}

		public void Save(Profile profile)
		{
			Remove(profile.Id);
			_profiles.Add(profile);
		}


		public Profile Get(string provider, string id)
		{
			throw new NotImplementedException();
		}
	}
}