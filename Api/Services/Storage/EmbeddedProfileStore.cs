﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mrf.Models.Profiles;
using LiteDB;

namespace Api.Services.Storage
{
	public class EmbeddedProfileStore : IProfileStore, IDisposable
	{
		private readonly LiteDatabase _db;
		private readonly LiteCollection<Profile> _collection;

		public EmbeddedProfileStore(string connectionString)
		{
			_db = new LiteDatabase(connectionString);
			_collection = _db.GetCollection<Profile>("Profiles");
			_collection.EnsureIndex(P => P.Provider);
			_collection.EnsureIndex(P => P.Identifier);
		}

		public Profile Get(Guid id)
		{
			return _collection.FindById(id);
		}

		public IEnumerable<Profile> List(IEnumerable<Guid> ids)
		{
			var profiles = _collection
				.Find(Query.In("_id", ids.Select(I => new BsonValue(I)).ToArray()))
				.ToList();
			
			return profiles;
		}

		public void Remove(Guid id)
		{
			_collection.Delete(C => C.Id == id);
		}

		public void Save(Profile profile)
		{
			if (_collection.Exists(P => P.Id == profile.Id))
			{
				_collection.Update(profile);
			}
			else
			{
				_collection.Insert(profile);
			}
		}

		public Profile Get(string provider, string id)
		{
			var profile = _collection.Find(P => P.Provider == provider && P.Identifier == id, 0, 1)
				.FirstOrDefault();
			
			return profile;
		}

		public void Dispose()
		{
			_db.Dispose();
		}
	}
}