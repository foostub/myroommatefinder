﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Utilities
{
	public class Expression
	{
		public static void Truthy(Func<bool> expression, Action action)
		{
			if (expression())
			{
				action();
			}
		}
	}
}