﻿using Api.Models;
using Mrf.Models;
using Mrf.Models.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Utilities
{
	public static class Extensions
	{
		public static Account ToAccount(this Profile profile)
		{
			return new Account()
			{
				ProfileId = profile.Id,
				Location = profile.Location,
				Age = (int)(DateTime.UtcNow.Subtract(profile.Birthday).TotalDays / 365),
				AllowAllContact = profile.AllowAllContact,
				CarnivoreIndex = profile.CarnivoreIndex,
				CleanlinessIndex = profile.CleanlinessIndex,
				Description = profile.Description,
				FirstName = profile.FirstName,
				Gender = profile.Gender.ToString(),
				LastName = profile.LastName,
				MaxLease = profile.MaxLease,
				MaxRent = profile.MaxRent,
				MaxUtilities = profile.MaxUtilities,
				MoveInBy = profile.MoveInBy == DateTime.MinValue ? DateTime.UtcNow : profile.MoveInBy,
				Orientation = profile.Orientation.ToString(),
				OvernightVisitors = profile.OvernightVisitors,
				Pets = profile.Pets,
				ShowInSearchResults = profile.ShowInSearchResults,
				Smoker = profile.Smoker,
				SocialIndex = profile.SocialIndex,
				Title = profile.Title,
				ToBedHour = profile.ToBedHour,
				WakeUpHour = profile.WakeUpHour,
				NoPets = profile.NoPets,
				MaxDeposit = profile.MaxDeposit
			};
		}

		public static double Cosine(this double[] v1, double[] v2)
		{
			var cosine = v1.Inner(v2) / (v1.Norm() * v2.Norm());
			if (double.IsNaN(cosine))
			{
				cosine = 0.0;
			}
			return cosine;
		}

		public static double Inner(this double[] v1, double[] v2)
		{
			double r = 0;
			for (int i = 0; i < v1.Length; i++)
			{
				r += v1[i] * v2[i];
			}
			return (float)r;
		}

		public static double Norm(this double[] v1)
		{
			double r = 0;
			for (int i = 0; i < v1.Length; i++)
			{
				r += System.Math.Pow(v1[i], 2);
			}
			r = System.Math.Pow(r, 0.5);
			return (float)r;
		}

		public static void Sum(this double[] v1, double[] v2)
		{
			for (var i = 0; i < v1.Length; i++)
			{
				v1[i] = (v1[i] + v2[i]) / 2;
			}
		}

		public static void Substract(this double[] v1, double[] v2)
		{
			for (var i = 0; i < v1.Length; i++)
			{
				v1[i] = (v1[i] - v2[i]) / 2;
			}
		}

		public static double Scale(this double value, double newStart, double newEnd, double originalStart, double originalEnd)
		{
			var scale = (newEnd - newStart) / (originalEnd - originalStart);
			return (newStart + ((value - originalStart) * scale));
		}

		public static double Scale(this int value, double newStart, double newEnd, double originalStart, double originalEnd)
		{
			return ((double)value).Scale(newStart, newEnd, originalStart, originalEnd);
		}
	}
}