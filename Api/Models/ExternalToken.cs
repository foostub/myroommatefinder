﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Api.Models
{
	public class ExternalToken
	{
		[Required]
		public string Provider { get; set; }
		[Required]
		public string Token { get; set; }
	}
}