﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
	public class ScoredAccount 
	{
		public double Score { get; set; }
		public double Distance { get; set; }
		public Account Account { get; set; }
		public Guid ProfileId { get; set; }
	}
}