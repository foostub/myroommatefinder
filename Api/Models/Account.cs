﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
	public class Account
	{
		public Guid ProfileId { get; set; }
		public string Location { get; set; }
		public int Age { get; set; }
		public string Gender { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Orientation { get; set; }
		public DateTime MoveInBy { get; set; }
		public int MaxLease { get; set; }
		public int MaxRent { get; set; }
		public int MaxUtilities { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public int SocialIndex { get; set; }
		public int OvernightVisitors { get; set; }
		public int Smoker { get; set; }
		public int CleanlinessIndex { get; set; }
		public int CarnivoreIndex { get; set; }
		public List<string> Pets { get; set; }
		public int WakeUpHour { get; set; }
		public int ToBedHour { get; set; }
		public bool ShowInSearchResults { get; set; }
		public bool AllowAllContact { get; set; }
		public bool NoPets { get; set; }
		public int MaxDeposit { get; set; }
	}
}