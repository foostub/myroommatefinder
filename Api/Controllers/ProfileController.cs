﻿using Api.Models;
using Api.Services.Search;
using Api.Services.Storage;
using Mrf.Models;
using Mrf.Models.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Api.Utilities;

namespace Api.Controllers
{
	//[Authorize]
	public class ProfileController : ApiController
	{
		private readonly SearchService _searchService;
		private readonly IProfileStore _profileStore;

		public ProfileController(SearchService searchService, IProfileStore profileStore)
		{
			_searchService = searchService;
			_profileStore = profileStore;
		}

		[HttpPut, Route("v1/api/profile")]
		public IHttpActionResult Update(Account account)
		{
			var identity = this.User.Identity as ClaimsIdentity;
			var profile = _profileStore.Get(new Guid(identity.Name));
			if (profile != null)
			{
				if (identity.Name != profile.Id.ToString())
				{
					return this.Unauthorized();
				}

				profile.AllowAllContact = account.AllowAllContact;
				profile.CarnivoreIndex = account.CarnivoreIndex;
				profile.CleanlinessIndex = account.CleanlinessIndex;
				profile.Description = account.Description;
				profile.Location = account.Location;
				profile.MaxLease = account.MaxLease;
				profile.MaxRent = account.MaxRent;
				profile.MaxUtilities = account.MaxUtilities;
				profile.MoveInBy = account.MoveInBy;
				Orientation orientation = Orientation.Undisclosed;
				if (Enum.TryParse<Orientation>(account.Orientation, out orientation))
				{
					profile.Orientation = orientation;
				}
				profile.OvernightVisitors = account.OvernightVisitors;
				profile.Pets = account.Pets;
				profile.ShowInSearchResults = account.ShowInSearchResults;
				profile.Smoker = account.Smoker;
				profile.SocialIndex = account.SocialIndex;
				profile.Title = account.Title;
				profile.ToBedHour = account.ToBedHour;
				profile.WakeUpHour = account.WakeUpHour;
				profile.NoPets = account.NoPets;
				profile.MaxDeposit = account.MaxDeposit;

				if (profile.ShowInSearchResults)
				{
					_searchService.AddProfile(profile);
				}
				else
				{
					_searchService.RemoveProfile(profile.Id);
				}
				_profileStore.Save(profile);
				
				return this.Ok();
			}
			else
			{
				return this.NotFound();
			}
		}

		[HttpGet, Route("v1/api/profile")]
		public IHttpActionResult Get()
		{
			var identity = this.User.Identity as ClaimsIdentity;
			var profile = _profileStore.Get(new Guid(identity.Name));
			if (profile == null)
			{
				return this.NotFound();
			}
			else
			{
				return this.Ok(profile.ToAccount());
			}
		}

		[HttpPost, Route("v1/api/profile/search")]
		public IHttpActionResult Search(ProfileSearch search)
		{
			//var identity = this.User.Identity as ClaimsIdentity;
			//var profile = _profileStore.Get(new Guid(identity.Name));
			//if (profile == null)
			//{
			//	return this.NotFound();
			//}

			search.Latitude = 47.8079;
			search.Longitude = -122.3601;

			var searchResults = _searchService.SearchProfiles(search);
			var scoredAccounts = new List<ScoredAccount>();
			for (var i = 0; i < searchResults.DocumentIds.Length; i++)
			{
				scoredAccounts.Add(new ScoredAccount()
				{
					ProfileId = new Guid(searchResults.DocumentIds[i]),
					Score = searchResults.Scores[i],
					Distance = searchResults.Distances[i]
				});
			}

			var results = _profileStore.List(searchResults.DocumentIds.Select(D => new Guid(D)))
				.Join(scoredAccounts, L => L.Id, R => R.ProfileId, (L, R) =>
				{
					R.Account = L.ToAccount();
					return R;
				})
				.OrderByDescending(R => R.Score)
				.ToList();

			return this.Ok(new PagedDataFacets()
			{
				Results = results,
				Start = search.Start,
				Total = searchResults.Total,
				PageSize = 10,
				HasPreviousPage = search.Start > 0,
				HasNextPage = search.Start + 10 < searchResults.Total,
				Facets = searchResults.Facets
			});
		}
	}
}
