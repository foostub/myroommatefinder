﻿using Api.Models;
using Api.Services.Authentication;
using Api.Services.Search;
using Api.Services.Storage;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Mrf.Models.Profiles;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace Api.Controllers
{
    public class AuthorizationController : ApiController
    {
		private readonly OAuthAuthorizationServerOptions _oauthServerOptions;
		private readonly SearchService _searchService;
		private readonly IProfileStore _profileStore;

		public AuthorizationController(OAuthAuthorizationServerOptions oauthServerOptions, SearchService searchService, IProfileStore profileStore)
		{
			_oauthServerOptions = oauthServerOptions;
			_searchService = searchService;
			_profileStore = profileStore;
		}

		[HttpGet(), Route("v1/oauth/facebook")]
		public IHttpActionResult Facebook()
		{
			return new ChallengeResult("Facebook", this, "/v1/oauth/facebook/success");
		}

		[OverrideAuthentication]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
		[HttpGet(), Route("v1/oauth/facebook/success")]
		public IHttpActionResult Success()
		{
			var identity = this.User.Identity as ClaimsIdentity;
			var externalToken = identity.FindFirstValue("accessToken");

			if (this.User.Identity.IsAuthenticated)
			{
				var birthDay = identity.FindFirstValue("Birthday");
				var gender = identity.FindFirstValue("Gender");
				var firstName = identity.FindFirstValue("FirstName");
				var lastName = identity.FindFirstValue("LastName");
				var location = identity.FindFirstValue("Location");
				var provider = identity.FindFirstValue("Provider");
				var id = identity.FindFirstValue("Id");

				var profile = _profileStore.Get(provider, id);
				if (profile == null)
				{
					profile = new Mrf.Models.Profiles.Profile();
					profile.Id = Guid.NewGuid();
					profile.Identifier = id;
					profile.Provider = provider;
					var bd = DateTime.UtcNow;
					if (DateTime.TryParse(birthDay, out bd))
					{
						profile.Birthday = bd;
					}
					Gender ge = Gender.Unknown;
					if (Enum.TryParse<Gender>(gender, true, out ge))
					{
						profile.Gender = ge;
					}
					profile.FirstName = firstName;
					profile.LastName = lastName;
					profile.Location = location;
					profile.Provider = provider;
					profile.Identifier = id;
					_profileStore.Save(profile);
				}
			}

			return this.Redirect(ConfigurationManager.AppSettings["WebApp.Endpoint"] + "#/status/" + externalToken);
		}

		[OverrideAuthentication]
		[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
		[HttpPost, Route("v1/oauth/token")]
		public async Task<IHttpActionResult> Token(ExternalToken token)
		{
			if (!ModelState.IsValid)
			{
				return this.BadRequest(ModelState);
			}

			var userName = "";

			// authorize external token
			var appToken = ConfigurationManager.AppSettings["Facebook.AppToken"];
			var facebookUrl = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", token.Token, appToken);
			var client = new HttpClient();
            var uri = new Uri(facebookUrl);
            var response = await client.GetAsync(uri);

			if (response.IsSuccessStatusCode)
			{
				var content = await response.Content.ReadAsStringAsync();
				dynamic authResponse = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(content);
				var isValid = (bool)authResponse["data"]["is_valid"];
				if (isValid)
				{
					var userId = (string)authResponse["data"]["user_id"];
					var appId = (string)authResponse["data"]["app_id"];

					var externalProfile = this.User.Identity as ClaimsIdentity;
					var provider = "Facebook";
					var profile = _profileStore.Get(provider, userId);

					userName = profile.Id.ToString();
				}
				else
				{
					return this.Unauthorized();
				}
			}
			else
			{
				return this.Unauthorized();
			}

			var identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

			identity.AddClaim(new Claim(ClaimTypes.Name, userName));

			var tokenExpiration = new TimeSpan(2, 0, 0, 0);

			var props = new AuthenticationProperties()
			{
				IssuedUtc = DateTime.UtcNow,
				ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration)
			};

			var ticket = new AuthenticationTicket(identity, props);

			var accessToken = _oauthServerOptions.AccessTokenFormat.Protect(ticket); 

			var tokenResponse = new JObject(
				new JProperty("userName", userName),
				new JProperty("access_token", accessToken),
				new JProperty("token_type", "bearer"),
				new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
				new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
				new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString()),
				new JProperty("roles", new string[] { "User" }),
				new JProperty("firstTime", true)
			);

			return this.Ok(tokenResponse);
		}
    }
}
