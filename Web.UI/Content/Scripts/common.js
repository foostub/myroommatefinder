﻿$(function () {
	$(document).on("click", "a.expand-panel", function (e) {
		var panelBody = $(this).closest(".panel").find(".panel-body");
		panelBody.toggle();
	});
})