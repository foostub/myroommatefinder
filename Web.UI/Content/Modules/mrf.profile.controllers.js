﻿angular.module("MRF.Profile.Controllers", ["ui.router"])
	.controller("Profile.Home", function ($scope, $state, $stateParams, profileApi) {
		$scope.tabs = [
			{ heading: "About", route: "profile.about" },
			{ heading: "Privacy", route: "profile.privacy" }
		];
	})
	.controller("Profile.About", function ($scope, $state, $stateParams, profileApi) {
		$scope.warningText = "";
		$scope.successText = "";

		$scope.checkPet = function (pet) {
			var idx = $scope.profile.pets.indexOf(pet);
			if (idx > -1) {
				$scope.profile.pets.splice(idx, 1);
			}
			else {
				$scope.profile.pets.push(pet);
			}
		};

		profileApi.get()
			.success(function (data) {
				$scope.profile = data;
				$scope.profile.pets = data.pets || [];
				if ($scope.profile.wakeUpHour === 0 && $scope.profile.toBedHour === 0) {
					$scope.profile.wakeUpHour = 6;
					$scope.profile.toBedHour = 22;
				}
			})
			.error(function () {
				$scope.warningText = "An error occurred while retrieving your profile.";
			});

		$scope.save = function () {
			$scope.successText = "";
			$scope.warningText = "";
			profileApi.update($scope.profile)
				.success(function (data) {
					$scope.successText = "Your profile was successfully saved.";
				})
				.error(function () {
					$scope.warningText = "An error occurred attempting to save your profile.";
				});
		};
	})
	.controller("Profile.Privacy", function ($scope, $state, $stateParams, profileApi) {
		$scope.warningText = "";
		$scope.successText = "";

		profileApi.get()
			.success(function (data) {
				$scope.profile = data;
			})
			.error(function () {
				$scope.warningText = "An error occurred while retrieving your profile.";
			});

		$scope.save = function () {
			$scope.successText = "";
			$scope.warningText = "";
			profileApi.update($scope.profile)
				.success(function (data) {
					$scope.successText = "Your profile was successfully saved.";
				})
				.error(function () {
					$scope.warningText = "An error occurred attempting to save your profile.";
				});
		};
	})
;

