﻿angular.module("MRF.Authentication", ["ui.router", "MRF.Authentication.Controllers"])
	.factory("authenticationApi", function ($http, serviceUrl) {
		return {
			authenticate: function (provider, token) {
				return $http.post(serviceUrl + "v1/oauth/token", { Provider: provider, Token: token });
				//var data = "grant_type=password&username=" + userName + "&password=" + password;
				//return $http.post(serviceUrl + "/token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } });
			}
		};
	})
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("authenticate", {
				url: "/authenticate",
				templateUrl: "/content/modules/views/authentication/signin.html",
				controller: "Authentication.SignIn"
			})
			.state("signout", {
				url: "/signout",
				templateUrl: "/content/modules/views/authentication/status.html",
				controller: "Authentication.SignOut"
			})
			.state("status", {
				url: "/status/:token",
				templateUrl: "/content/modules/views/authentication/status.html",
				controller: "Authentication.Status"
			})
		;
	});

