﻿angular.module("MRF.Profile", ["ui.router", "ui.router.tabs", "MRF.Profile.Controllers"])
	.factory("profileApi", function ($http, principal, serviceUrl) {
		return {
			get: function () {
				return $http.get(serviceUrl + "v1/api/profile", {
					headers: { "Authorization": "Bearer " + principal.token() }
				});
			},
			update: function (profile) {
				return $http({
					method: "PUT",
					url: serviceUrl + "v1/api/profile",
					data: profile,
					headers: {
						"Content-Type": "application/json",
						"Authorization": "Bearer " + principal.token()
					}
				});
			}
		};
	})
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("profile", {
				url: "/profile",
				parent: "restricted",
				templateUrl: "/content/modules/views/profile/home.html",
				controller: "Profile.Home"
			})
			.state("profile.about", {
				url: "/about",
				templateUrl: "/content/modules/views/profile/about.html",
				controller: "Profile.About"
			})
			.state("profile.privacy", {
				url: "/privacy",
				templateUrl: "/content/modules/views/profile/privacy.html",
				controller: "Profile.Privacy"
			})
			.state("profile.place", {
				url: "/place",
				templateUrl: "/content/modules/views/profile/place.html",
				controller: "Profile.Place"
			})
		;
	});

