﻿angular.module("MRF.Messages", ["ui.router", "ui.router.tabs", "MRF.Messages.Controllers"])
	.factory("messagesApi", function ($http) {
		return {
			list: function () {
				return $http.get("/data/messages.json");
			}
		};
	})
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("messages", {
				parent: "restricted",
				url: "/messages",
				templateUrl: "/content/modules/views/messages/home.html",
				controller: "Messages.Home"
			})
		;
	});

