﻿angular.module("MRF.Messages.Controllers", ["ui.router"])
	.controller("Messages.Home", function ($scope, $state, $stateParams, messagesApi) {
		$scope.messages = [];
		messagesApi.list()
			.success(function (data) {
				$scope.messages = data;
			});
	})
;

