﻿angular.module("MRF.Matches", ["ui.router", "ui.router.tabs", "MRF.Matches.Controllers", "ui.slider"])
	.factory("matchesApi", function ($http, principal, serviceUrl) {
		return {
			people: function (criteria) {
				return $http({
					method: "POST",
					url: serviceUrl + "v1/api/profile/search",
					data: criteria,
					headers: {
						"Content-Type": "application/json",
						"Authorization": "Bearer " + principal.token()
					}
				});
			}
		};
	})
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("matches", {
				parent: "restricted",
				url: "/matches",
				templateUrl: "/content/modules/views/matches/home.html",
				controller: "Matches.Home"
			})
			.state("matches.people", {
				url: "/people",
				templateUrl: "/content/modules/views/matches/people.html",
				controller: "Matches.People"
			})
		;
	})
	.filter("humanCamel", function () {
		return function (input) {
			var words = input.split(/(?=[A-Z])/);
			return words.join(" ");
		};
	})
;


