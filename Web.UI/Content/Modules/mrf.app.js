﻿angular.module("MRF", ["ui.router", "ui.bootstrap", "ngStorage", "MRF.Profile", "MRF.Matches", "MRF.Authentication", "MRF.Messages"])
	.config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise("/");
		$stateProvider
			.state("home", {
				url: "/",
				parent: "restricted"
				//templateUrl: "/content/modules/views/home.html",
				//controller: function ($scope, $state) {
				//	
				//}
			})
			.state("restricted", {
				abstract: true,
				templateUrl: "content/modules/views/common/restricted.html",
				resolve: {
					authorize: function (authorization) {
					  	return authorization.authorize();
					}
				},
				data: {
					roles: []//["User"]
				}
			})
		;
	})
	.factory("identityApi", function ($http) {
		return {
			verify: function () {
				return $http.get("/data/matches.people.json");
			}
		};
	})
	.factory("principal", function ($q, $sessionStorage) {
		var _identity = undefined,
		  _authenticated = false;

		return {
			token: function () {
				if (this.isAuthenticated()) {
					return $sessionStorage.identity.access_token;
				} else {
					return "";
				}
			},
			signOut: function () {
				_authenticated = false;
				_identity = null;
				$sessionStorage.identity = null;
			},
			isIdentityResolved: function () {
				return angular.isDefined(_identity);
			},
			isAuthenticated: function () {
				return _authenticated;
			},
			isInRole: function (role) {
				if (!_authenticated || !_identity.roles) return false;

				return _identity.roles.indexOf(role) != -1;
			},
			isInAnyRole: function (roles) {
				if (!_authenticated || !_identity.roles) return false;

				for (var i = 0; i < roles.length; i++) {
					if (this.isInRole(roles[i])) return true;
				}

				return false;
			},
			authenticate: function (identity) {
				_identity = identity;
				_authenticated = identity != null;
				$sessionStorage.identity = identity;
			},
			identity: function (force) {
				var deferred = $q.defer();

				if (force === true) _identity = undefined;

				// check and see if we have retrieved the identity data from the server. 
				// if we have, reuse it by immediately resolving
				if (angular.isDefined(_identity)) {
					deferred.resolve(_identity);

					return deferred.promise;
				}

				/*
				var self = this;
				$timeout(function () {
					self.authenticate(null);
					deferred.resolve(_identity);
				}, 1000);
				*/

				this.authenticate($sessionStorage.identity);

				deferred.resolve(_identity);

				return deferred.promise;
			}
		};
	})
	.factory("authorization", function ($rootScope, $state, principal) {
		return {
			authorize: function () {
				return principal.identity()
				  .then(function () {
				  	var isAuthenticated = principal.isAuthenticated();

				  	if ($rootScope.toState.data && $rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !principal.isInAnyRole($rootScope.toState.data.roles)) {
				  		if (isAuthenticated) {
				  			$state.go("accessdenied"); // user is signed in but not authorized for desired state
				  		} else {
				  			// user is not authenticated. stow the state they wanted before you
				  			// send them to the signin state, so you can return them when you're done
				  			$rootScope.returnToState = $rootScope.toState;
				  			$rootScope.returnToStateParams = $rootScope.toStateParams;

				  			// now, send them to the signin state so they can log in
				  			$state.go("authenticate");
				  		}
				  	}
				  });
			}
		};
	})
	.constant("serviceUrl", "http://localhost:4275/")
	.directive("blockProfile", function () {
		return {
			restrict: "EA",
			controller: function ($scope, $modal) {
				$scope.clicked = function () {
					$modal.open({
						templateUrl: "/content/modules/views/common/modal.html",
						controller: function ($scope, $modalInstance) {
							$scope.title = "Block Profile";
							$scope.question = "Are you sure you want to block this profile?"
							$scope.ok = function () {
								$modalInstance.close($scope.item);
							};
							$scope.cancel = function () {
								$modalInstance.dismiss("cancel");
							};
						},
						resolve: {
							item: function () {
								return $scope.selected;
							}
						}
					})
					.result
						.then(function (data) {
							alert(data);
						}, function () {
							alert('dismissed');
						});
				};
			},
			link: function ($scope, element, attrs) {
				element.bind("click", $scope.clicked)
			}
		};
	})
	.directive("removeProfile", function () {
		return {
			restrict: "EA",
			controller: function ($scope, $modal) {
				$scope.clicked = function () {
					$modal.open({
						templateUrl: "/content/modules/views/common/modal.html",
						controller: function ($scope, $modalInstance) {
							$scope.title = "Remove Profile";
							$scope.question = "Are you sure you want to remove this profile?"
							$scope.ok = function () {
								$modalInstance.close($scope.item);
							};
							$scope.cancel = function () {
								$modalInstance.dismiss("cancel");
							};
						},
						resolve: {
							item: function () {
								return $scope.selected;
							}
						}
					})
					.result
						.then(function (data) {
							alert(data);
						}, function () {
							alert('dismissed');
						});
				};
			},
			link: function ($scope, element, attrs) {
				element.bind("click", $scope.clicked)
			}
		};
	})
	.directive("contactProfile", function () {
		return {
			restrict: "EA",
			controller: function ($scope, $modal) {
				$scope.clicked = function () {
					$modal.open({
						templateUrl: "/content/modules/views/common/contact.html",
						controller: function ($scope, $modalInstance) {
							$scope.ok = function () {
								$modalInstance.close($scope.item);
							};
							$scope.cancel = function () {
								$modalInstance.dismiss("cancel");
							};
						},
						resolve: {
							item: function () {
								return $scope.selected;
							}
						}
					})
					.result
						.then(function (data) {
							alert(data);
						}, function () {
							alert('dismissed');
						});
				};
			},
			link: function ($scope, element, attrs) {
				element.bind("click", $scope.clicked)
			}
		};
	})
	.controller("MRF.Header", function ($scope, $location) {
		$scope.isActive = function (viewLocation) {
			return $location.path().indexOf(viewLocation) === 0;
		};
	})
	.run(function ($rootScope, $state, $stateParams, authorization, principal) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		$rootScope.$on("$stateChangeStart", function (event, toState, toStateParams) {
			$rootScope.toState = toState;
			$rootScope.toStateParams = toStateParams;
			if (principal.isIdentityResolved()) {
				authorization.authorize();
			}
		});
	});

