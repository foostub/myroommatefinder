﻿angular.module("MRF.Authentication.Controllers", ["ui.router"])
	.controller("Authentication.SignIn", function ($scope, $state, $stateParams, authenticationApi, $rootScope) {

	})
	.controller("Authentication.SignOut", function ($scope, $state, $stateParams, $rootScope, principal) {
		principal.signOut();
		$state.go("authenticate");
	})
	.controller("Authentication.Status", function ($scope, $state, $stateParams, authenticationApi, principal) {
		$scope.warningText = "";
		authenticationApi.authenticate("Facebook", $stateParams.token)
			.success(function (data) {
				principal.authenticate(data);
				$state.go("matches.people");
			})
			.error(function (err, status) {
				$scope.warningText = "We are unable to authorize your account at this time.";
			});
	})
;

