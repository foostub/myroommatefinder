﻿angular.module("MRF.Matches.Controllers", ["ui.router"])
	.controller("Matches.Home", function ($scope, $state, $stateParams, profileApi, $rootScope) {
		$scope.tabs = [
			{ heading: "People", route: "matches.people" },
		];
	})
	.controller("Matches.People", function ($scope, $state, $stateParams, matchesApi, $sessionStorage, $window) {
		$scope.isLoading = true;
		$scope.warningText = "";

		$scope.criteria = $sessionStorage.criteria || {
			start: 0,
			moveInByDays: 0,
			moveInBy: new Date(),
			age: 25,
			wakeUpHour: 6,
			toBedHour: 22,
			socialIndex: 3,
			overnightVisitors: 3,
			smoking: 1,
			cleanlinessIndex: 3,
			carnivoreIndex: 3,
			filters: []
		};

		var birthDay = new Date();
		birthDay.setFullYear(birthDay.getFullYear() - $scope.criteria.age);
		$scope.criteria.birthDay = birthDay;

		$scope.$watch("criteria.age", function (newValue, oldValue) {
			var birthDay = new Date();
			birthDay.setFullYear(birthDay.getFullYear() - $scope.criteria.age);
			$scope.criteria.birthDay = birthDay;
			if (!$scope.isLoading) {
				$scope.search();
			}
		});

		$scope.$watch("criteria.socialIndex", function (newValue, oldValue) {
			if (!$scope.isLoading) {
				$scope.search();
			}
		});

		$scope.$watch("criteria.overnightVisitors", function (newValue, oldValue) {
			if (!$scope.isLoading) {
				$scope.search();
			}
		});

		$scope.$watch("criteria.smoker", function (newValue, oldValue) {
			if (!$scope.isLoading) {
				$scope.search();
			}
		});

		$scope.$watch("criteria.cleanlinessIndex", function (newValue, oldValue) {
			if (!$scope.isLoading) {
				$scope.search();
			}
		});

		$scope.$watch("criteria.moveInByDays", function (newValue, oldValue) {
			$scope.criteria.moveInBy = new Date(Date.now() + 864e5 * newValue);
			if (!$scope.isLoading) {
				$scope.search();
			}
		});

		$scope.people = {
			items: [],
			facets: []
		};

		$scope.search = function () {
			$scope.isLoading = true;
			$window.scrollTo(0, 0);
			$scope.warningText == "";
			$sessionStorage.criteria = $scope.criteria;
			matchesApi.people($scope.criteria)
				.success(function (data) {
					$scope.people = data;
					$scope.isLoading = false;
				})
				.error(function () {
					$scope.isLoading = false;
					$scope.warningText = "An error occurred attempting to search.";
				});
		};

		$scope.previous = function () {
			$scope.criteria.start = $scope.criteria.start - 10;
			$scope.search();
		};

		$scope.next = function () {
			$scope.criteria.start = $scope.criteria.start + 10;
			$scope.search();
		};

		$scope.addFilter = function (facet, filterValue) {
			$scope.criteria.filters.push({ name: facet.name, value: filterValue.value, lower: filterValue.lower, upper: filterValue.upper });
			$scope.criteria.start = 0;
			$scope.search();
		}

		$scope.removeFilter = function (filter) {
			var index = $scope.criteria.filters.indexOf(filter);
			if (index > -1) {
				$scope.criteria.filters.splice(index, 1);
			}
			$scope.criteria.start = 0;
			$scope.search();
		}

		$scope.search();
	})
;

