﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrf.Models
{
	public class PagedData<T>
	{
		public List<T> Results { get; set; }
		public int Start { get; set; }
		public int PageSize { get; set; }
		public int Total { get; set; }
		public bool HasPreviousPage { get; set; }
		public bool HasNextPage { get; set; }
	}
}
