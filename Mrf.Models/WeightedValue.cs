﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrf.Models
{
	public class WeightedValue<T> : IEquatable<WeightedValue<T>> where T : IEquatable<T>
	{
		public T Value { get; set; }
		public int Weight { get; set; }

		public WeightedValue()
		{

		}

		public WeightedValue(T value, int weight)
		{
			this.Value = value;
			this.Weight = weight;
		}

		bool IEquatable<WeightedValue<T>>.Equals(WeightedValue<T> other)
		{
			return other.Value.Equals(this.Value);
		}

		public static implicit operator WeightedValue<T>(T d)
		{
			return new WeightedValue<T>
			{
				Value = d
			};
		}
	}
}
