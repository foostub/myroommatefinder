﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrf.Models.Profiles
{
	public class ProfileSearch 
	{
		public string Keywords { get; set; }
		public int Start { get; set; }
		public DateTime? MoveInBy { get; set; }
		public DateTime? Birthday { get; set; }
		public int? SocialIndex { get; set; }
		public int? OvernightVisitors { get; set; }
		public int? Smoker { get; set; }
		public int? CleanlinessIndex { get; set; }
		public int? CarnivoreIndex { get; set; }
		public int? WakeUpHour { get; set; }
		public int? ToBedHour { get; set; }
		//public bool ShowInSearchResults { get; set; }
		public bool AllowAllContact { get; set; }
		public bool NoPets { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public List<Guid> ExcludeIds { get; private set; }
		public List<Filter> Filters { get; set; }

		public ProfileSearch()
		{
			this.ExcludeIds = new List<Guid>();
			this.Filters = new List<Filter>();
		}

		public class Filter
		{
			public string Name { get; set; }
			public string Value { get; set; }
			public int? Lower { get; set; }
			public int? Upper { get; set; }
		}
	}
}
