This is a proof of concept roommate matching service I wrote using c#, lucene.net, web api, and angular. I started the facebook integration for registration and authorization but only had it running on localhost. I focused most of the effort on the search (demo at https://mrf-ui.azurewebsites.net/#/matches/people). The rest of the app is using mock json files I used for building the u/i. 

If you're going to run it locally you'll need to update  
```
#!javascript

.constant("serviceUrl", "http://localhost:4275/")
```
 in the [u/i project](https://bitbucket.org/foostub/myroommatefinder/src/8d0e4ad98deab5351c077af87e7d42ebf101fc0d/Web.UI/Content/Modules/mrf.app.js?at=master&fileviewer=file-view-default#mrf.app.js-129). Feel free to send questions or comments to llah.d.nevets@gmail.com. 